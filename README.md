# Attribute Template plugins for kanka.io

A rough and messy local workspace to approximate Kanka's layout and css capabilities for attribute template development.

It isn't perfect, but is a *lot* more convenient than the copy-paste-update-check-repeat on the kanka plugin page. 

## Features
* basic css pulled from Kanka.io and local copies of font-awesome and rpg-awesome.
* js to find @ logic and replace it with placeholder text for testing layouts while keeping "Blade" syntax in the files.
* a copy of bootstrap grid 4.6 that is namespaced with "ns-" so it won't clash with the kanka version.

### KNOWN ISSUES
* Included CSS is out of date and needs to be updated since the Nov. patch.
* Layout widths can vary based on other themes